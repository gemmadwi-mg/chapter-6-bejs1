require('dotenv').config();
const { promisify } = require('util');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { UserGame } = require('../models');
const Email = require('./../utils/email');
const controller = {};

const { SALT_ROUNDS } = process.env;

const signToken = id => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
}


const checkPassword = (password, hashedPassword) => bcrypt.compareSync(password, hashedPassword);

controller.signup = async (req, res, next) => {

    try {
        const newUser = await UserGame.create({
            username: req.body.username,
            password: req.body.password,
            email: req.body.email,
            role: req.body.role
        });

        await new Email(newUser).sendWelcome();

        const token = signToken(newUser.user_game_id);

        res.status(201).json({
            status: 'success',
            token,
            data: {
                user: newUser
            }
        })
    } catch (error) {
        res.status(404).json({
            status: 'fail',
            message: error
        })
    }
}

controller.login = async (req, res, next) => {
    try {
        const { email, password } = req.body;

        if (!email || !password) {
            return res.status(400).json({
                status: "error",
                message: "Please provide email and password!"
            })
        }

        const foundUser = await UserGame.findOne({ where: { email: email } });

        if (!foundUser) throw { message: 'User Not Found', status: 'failed', code: 404 };

        const isValidPassword = bcrypt.compareSync(password, foundUser.password);

        if (isValidPassword) {

            const token = signToken(foundUser.user_game_id);

            return res.status(200).json({
                status: 'success',
                token
            })
        } else { 
            return res.status(404).json({
                status: 'error',
                message: "Wrong password"
            })
        }

    } catch (error) {
        res.status(404).json({
            status: 'fail',
            message: error
        })
    }
}

controller.protect = async (req, res, next) => {
    let token;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        token = req.headers.authorization.split(' ')[1];
    }

    if (!token) {

        return next(
            res.status(401).json({
                status: "error",
                message: "You are not logged in! Please log in to get access."
            })
        );
    }

    // // Verification token
    const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

    // Check if user still exists
    const currentUser = await UserGame.findByPk(decoded.id);

    if (!currentUser) {
        return next(
            res.status(401).json({
                status: "error",
                message: "The user belonging to this token does no longer exist."
            })
        );
    }

    req.user = currentUser;
    next()
}

controller.restrictTo = (...roles) => {
    return (req, res, next) => {
        // roles ['admin', 'lead-guide']. role='user'
        if (!roles.includes(req.user.role)) {
            return next(
                res.status(401).json({
                    status: "error",
                    message: "You do not have permission to perform this action"
                })
            );
        }

        next();
    };
};

controller.getOtp = async (req, res, next) => {
    try {
        // Get email from body request
        // 1) Get user based on POSTed email
        const user = await UserGame.findOne({ email: req.body.email });
        if (!user) {
            return next(
                res.status(404).json({
                    status: "error",
                    message: "There is no user with email address."
                })
            );
        }

        // Create unique OTP
        const otp = Math.random().toString(36).slice(-6).toUpperCase();

        // Hash generated OTP
        const hashedOtp = bcrypt.hashSync(otp, +SALT_ROUNDS);

        const updatedUser = await UserGame.update(
            {
                otp: hashedOtp
            },
            {
                where: {
                    email: user.email
                }
            }
        );

        if (!updatedUser[0]) throw { message: 'Email is not registered', status: 'Failed', code: 404 };

        const otpMessage = `Your OTP is ${otp}, please use this otp as your credential to retrieve the password`;

        await new Email(user).getOtp(otpMessage);

        return res.status(200).json({
            message: `OTP successfully sent to`
        });
    } catch (error) {
        res.status(404).json({
            status: 'fail',
            message: error
        })
    }
};

controller.forgetPassword = async (req, res, next) => {
    try {
        // Retrieve information from body request
        const { email, newPassword, newPasswordConfirmation, otp } = req.body;

        if (newPassword === newPasswordConfirmation) {
            // Search User
            const foundUser = await UserGame.findOne({
                where: {
                    email: email
                }
            });

            // Check if user exists
            if (!foundUser) throw { message: 'Email is not registered', status: 'Failed', code: 404 };

            // If User exists, check OTP
            const compareOtp = checkPassword(otp, foundUser.otp);

            if (compareOtp) {
                await foundUser.update({
                    password: bcrypt.hashSync(newPassword, +SALT_ROUNDS),
                    otp: null
                });

                return res.status(200).json({
                    status: 'Success',
                    message: 'Password successfully changed'
                });
            }

            throw { message: 'Wrong OTP', status: 'Failed', code: 400 };
        }

        throw { message: `Password doesn't match`, status: 'Failed', code: 400 };
    } catch (error) {
        if (error.code) {
            return res.status(error.code).json({
                status: error.status,
                message: error.message
            });
        }
        next(err);
    }
};

module.exports = controller;