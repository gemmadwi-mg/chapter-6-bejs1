const express = require('express');
const router = express.Router();
const controller = require('../controllers/index')
const validate = require('../middlewares/validate')
const validation = require('../validations/usergame')


router.post('/signup', controller.auth.signup);
router.post('/login', controller.auth.login);

// Untuk mendapatkan OTP
router.post('/get-otp', controller.auth.getOtp);

router.post('/forget-password', controller.auth.forgetPassword);

router.get('/', controller.usergame.getAll);
router.post('/',
    controller.auth.protect,
    controller.auth.restrictTo('admin'),
    validation.create(), validate,
    controller.usergame.post);
router.get('/:user_game_id',
    validation.findById(), validate,
    controller.usergame.getUserGameById);
router.put('/:user_game_id',
    controller.auth.protect,
    controller.auth.restrictTo('admin'),
    validation.update(),
    validate, controller.usergame.put);
router.delete('/:user_game_id',
    controller.auth.protect,
    controller.auth.restrictTo('admin'),
    validation.destroy(),
    validate,
    controller.usergame.delete);

module.exports = router;