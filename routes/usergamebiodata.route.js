const express = require('express');
const router = express.Router();
const controller = require('../controllers/index');
const validate = require('../middlewares/validate');
const validation = require('../validations/usergamebiodata');

const upload = require('../misc/multer');
const uploadWithCloudinary = require('../misc/cloudinary');

router.get('/', controller.usergamebiodata.getAll);
router.post('/',
    controller.auth.protect,
    controller.auth.restrictTo('admin'),
    validation.create(), validate,
    controller.usergamebiodata.post);
router.get('/:user_game_biodata_id', validation.findById(), validate, controller.usergamebiodata.getUserGameBiodataById);
router.put('/:user_game_biodata_id',
    controller.auth.protect,
    controller.auth.restrictTo('admin'),
    validation.update(),
    validate,
    controller.usergamebiodata.put);
router.delete('/:user_game_biodata_id',
    controller.auth.protect,
    controller.auth.restrictTo('admin'),
    validation.destroy(),
    validate,
    controller.usergamebiodata.delete);

router.post('/uploadvideo', upload.single('profile_video'), uploadWithCloudinary, controller.usergamebiodata.uploadVideo);

module.exports = router;