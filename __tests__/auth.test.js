const request = require('supertest')
const app = require('../app')
const { sequelize } = require('../models');

const userGame = require('../masterdata/usergame.json').map((eachData) => {
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
})

beforeAll((done) => {
    sequelize.queryInterface
        .bulkInsert('UserGames', userGame, {})
        .then(() => {
            done()
        })
        .catch((err) => {
            done()
        })
})

describe('Usergame signup route', () => {
    describe('should return a 201 and create new usergame', () => {
        it('should be return 201', async () => {
            const usergame = {
                username: "gembox",
                password: 'gem123',
                email: 'ggembox@mail.com'
            }

            const res = await request(app).post("/api/v1/usergame/signup").send(usergame);

            expect(res.statusCode).toBe(201);

        })
    })
    describe("should return a 404 and doesn't create new usergame", () => {
        it('should be return an 404', async () => {
            const usergame = {
                username: "gembox",
                password: 'gem123',
                email: 'ggembox@mail.com'
            }

            const res = await request(app).post("/api/v1/usergame/signup").send(usergame);

            expect(res.statusCode).toBe(404);

        })
    })
})



describe('Usergame login route', () => {
    describe('when user login', () => {
        it('should be return 200 when success', async () => {
            const usergame = {
                email: 'ggembox@mail.com',
                password: 'gem123'
            }

            const res = await request(app).post("/api/v1/usergame/login").send(usergame);

            expect(res.statusCode).toBe(200);

        })
    })
})
