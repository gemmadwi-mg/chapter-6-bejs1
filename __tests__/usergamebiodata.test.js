require('dotenv').config();
const jwt = require('jsonwebtoken');
const app = require('../app');
const { sequelize, UserGame } = require('../models');


const request = require('supertest');

const usergamebiodata = require('../masterdata/usergamebiodata.json').map((eachData) => {
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
})

const userPayload = {
    username: "gembox",
    password: 'gem123',
    email: 'ggembox@mail.com'
};

const signToken = id => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
}

beforeAll((done) => {
    sequelize.queryInterface
        .bulkInsert('UserGameBiodata', usergamebiodata, {})
        .then(() => {
            done()
        })
        .catch((err) => {
            done()
        })
})

afterAll((done) => {
    sequelize.queryInterface
        .bulkDelete('UserGameBiodata', null, { truncate: true, restartIdentity: true })
        .then(() => {
            done()
        })
        .catch((err) => {
            done()
        })
})


describe("given the user is not logged in", () => {
    it("should return a 401", async () => {
        const { statusCode } = await request(app).post("/api/v1/usergamebiodata");

        expect(statusCode).toBe(401);
    });
});

describe("given the user is logged in", () => {
    describe("create usergamebiodata route", () => {
        it("should return a 201 and create usergamebiodata", async () => {
            const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

            const jwt = signToken(user.user_game_id);

            const usergamebiodatapayload = {
                usia: 16,
                user_game_id: 1
            }

            const { statusCode, body } = await request(app)
                .post("/api/v1/usergamebiodata")
                .set("Authorization", `Bearer ${jwt}`)
                .send(usergamebiodatapayload);

            expect(statusCode).toBe(201);
        });

        it("should return a 400", async () => {
            const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

            const jwt = signToken(user.user_game_id);

            const usergamebiodatapayload = {
                usia: "hahahwd",
                user_game_id: "hwhwhwhd"
            }

            const { statusCode, body } = await request(app)
                .post("/api/v1/usergamebiodata")
                .set("Authorization", `Bearer ${jwt}`)
                .send(usergamebiodatapayload);

            expect(statusCode).toBe(400);
        });

        it("should return a 400", async () => {
            const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

            const jwt = signToken(user.user_game_id);

            const usergamebiodatapayload = {
                usia: 16,
                user_game_id: "hahahah"
            }

            const { statusCode, body } = await request(app)
                .post("/api/v1/usergamebiodata")
                .set("Authorization", `Bearer ${jwt}`)
                .send(usergamebiodatapayload);

            expect(statusCode).toBe(400);
        });
    });

    describe("get all usergamebiodata route", () => {
        it("should return a 200 and the gamebiodata", async () => {

            const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

            const jwt = signToken(user.user_game_id);

            const { statusCode, body } = await request(app)
                .get("/api/v1/usergamebiodata")
                .set("Authorization", `Bearer ${jwt}`)

            expect(statusCode).toBe(200);

        });
    })

    describe("get by id usergamebiodata route", () => {
        describe("given the usergamebiodata does not exists", () => {
            it("should return a 404", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameBiodataId = "1234";
                const { body, statusCode } = await request(app).get(
                    `/api/v1/usergamebiodata/${userGameBiodataId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(404);
            });
        });

        describe("given the usergamebiodata does exists", () => {
            it("should return a 200 status and the usergamebiodata", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameBiodataId = "1"

                const { body, statusCode } = await request(app).get(
                    `/api/v1/usergamebiodata/${userGameBiodataId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(200);

            });
        });
    });

    describe("update usergamebiodata route", () => {
        describe("given the usergamebiodata does not exists", () => {
            it("should return a 404", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameBiodataId = "1234";
                const { body, statusCode } = await request(app).put(
                    `/api/v1/usergamebiodata/${userGameBiodataId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(404);
            });
        });

        describe("given the usergamebiodata does exists", () => {
            it("should return a 201 and update usergamebiodata", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGamebiodataId = "1";

                const usergamebiodatapayload = {
                    usia: 20,
                    user_game_id: 1
                }

                const { statusCode, body } = await request(app)
                    .put(`/api/v1/usergamebiodata/${userGamebiodataId}`)
                    .set("Authorization", `Bearer ${jwt}`)
                    .send(usergamebiodatapayload);

                expect(statusCode).toBe(201);
            });
        })


    });

    describe("delete usergamebiodata route", () => {
        describe("given the usergamebiodata does not exists", () => {
            it("should return a 404", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameBiodataId = "1234";
                const { body, statusCode } = await request(app).delete(
                    `/api/v1/usergamebiodata/${userGameBiodataId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(404);
            });


        });

        describe("given the usergamebiodata does exists", () => {
            it("should return a 201 and delete the usergamebiodata", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameBiodataId = "3";

                const { statusCode, body } = await request(app)
                    .delete(`/api/v1/usergamebiodata/${userGameBiodataId}`)
                    .set("Authorization", `Bearer ${jwt}`)

                expect(statusCode).toBe(201);
            });
        })


    });

})
