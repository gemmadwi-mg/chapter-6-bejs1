const multer = require('multer');
const fs = require('fs');

const storage = multer.diskStorage({
    filename: (req, file, cb) => {
        const fileType = file.mimetype.split('/')[1];
        cb(null, file.fieldname + '-' + Date.now() + `.${fileType}`);
    }
});

const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        // The function should call `cb` with a boolean
        // to indicate if the file should be accepted
        if (file.mimetype === 'video/mp4') return cb(null, true);
        // To reject this file pass `false`, like so:
        cb(null, false);

        // You can always pass an error if something goes wrong:
        cb(new Error('Only .mp4, .avi and .mov format allowed'));
    }
});

module.exports = upload;
