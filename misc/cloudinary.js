const cloudinary = require('cloudinary').v2;
const fs = require('fs');

cloudinary.config({
    cloud_name: 'dowmxp8ji',
    api_key: '399975762921698',
    api_secret: 'XsuDse1D5_qiAMAqAcH7Oljdzu8'
});

const uploadWithCloudinary = async (req, res, next) => {
    try {
        const foldering = `my-asset/video/${req.file.mimetype.split('/')[0]}`;
        const uploadResult = await cloudinary.uploader.upload(req.file.path, {
            folder: foldering,
            resource_type: "video",
            chunk_size: 6000000,
        });
        fs.unlinkSync(req.file.path);
        req.body.profile_video_url = uploadResult.secure_url;
        next();
    } catch (error) {
        fs.unlinkSync(req.file.path);
        console.log(error);
    }
};

module.exports = uploadWithCloudinary;
